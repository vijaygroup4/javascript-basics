//destructuring carInformation function from problem1.js
let carInformation = require("../problem1");

//importing inventory array
let inventory = require("./inventory");

//Problem 1
let car = carInformation(inventory, 33);
if (car) {
  console.log(
    `Car ${car.id} is a ${car.car_year} ${car.car_make} ${car.car_model}`
  );
} else {
  console.log(`Please check the array input and id`);
}
