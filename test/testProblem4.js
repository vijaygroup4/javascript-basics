//destructuring carYears function from problem4.js
let carYears = require("../problem4");

//importing inventory array
let inventory = require("./inventory");

//Problem 4
let car_Years = carYears(inventory);
if (car_Years) {
  console.log(car_Years);
} else {
  console.log("Please give valid array");
}
