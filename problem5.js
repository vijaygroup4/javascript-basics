//Function to find cars older than 2000
function oldCars(inventory) {
  if (!inventory) {
    return null;
  }
  let oldCarArray = [];
  for (car of inventory) {
    if (car.car_year < 2000) {
      oldCarArray.push(car.car_make);
    }
  }

  return oldCarArray;
}

//exporting the above function
module.exports = oldCars;
