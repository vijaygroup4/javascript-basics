//Function for sorting cars based on model-Problem 3
function sortCarModels(inventory) {
  if (!inventory) {
    return null;
  }
  let carModels = [];
  for (let car of inventory) {
    carModels.push(car.car_model);
  }

  //sorting the car models into lexicographic order
  carModels.sort();
  return carModels;
}

//exporting the above function
module.exports = sortCarModels;
