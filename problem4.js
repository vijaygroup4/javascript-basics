//Function to get car years -Problem 4
function carYears(inventory) {
  if (!inventory) {
    return null;
  }
  let carYears = [];
  for (let car of inventory) {
    carYears.push(car.car_year);
  }

  return carYears;
}

//exporting the above function
module.exports = carYears;
