//Function to find last car
function latestCar(inventory) {
  if (!inventory) {
    return null;
  }

  //returning the last car
  return inventory[inventory.length - 1];
}

//exporting the above function
module.exports = latestCar;
