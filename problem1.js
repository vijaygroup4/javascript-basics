//Function to find car information by id
function carInformation(inventory, id) {
  if (!inventory) {
    return null;
  }
  for (car of inventory) {
    if (car.id === Number(id)) {
      return car;
    }
  }
}

//exporting the above function
module.exports = carInformation;
