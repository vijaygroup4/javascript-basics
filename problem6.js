//Function to find bmw and Audi cars
function bmwAndAudiCars(inventory) {
  if (!inventory) {
    return null;
  }
  let bmwAndAudiArray = [];

  //filtering only BMW and Audi cars
  for (let car of inventory) {
    if (car.car_make === "BMW" || car.car_make === "Audi") {
      bmwAndAudiArray.push(car);
    }
  }
  return bmwAndAudiArray;
}

//exporting the above function
module.exports = bmwAndAudiCars;
